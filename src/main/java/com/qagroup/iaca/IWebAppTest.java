package com.qagroup.iaca;

public interface IWebAppTest {

	public IWebApp getTestedAppInstance();
}
