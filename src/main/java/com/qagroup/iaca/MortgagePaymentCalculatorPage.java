package com.qagroup.iaca;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public class MortgagePaymentCalculatorPage {

	private static final String purchase_price_section_css = "#calculateur-pret-hypotecaire .row:not(#zone_mise_de_fonds) .inputSlide-ia";

	@FindBy(css = purchase_price_section_css)
	private WebElement purchasePriceSection;

	@FindBys({ @FindBy(css = purchase_price_section_css), @FindBy(css = ".slider-handle.min-slider-handle") })
	private WebElement sliderHandle;

	@FindBys({ @FindBy(css = purchase_price_section_css), @FindBy(css = ".slider-track-high") })
	private WebElement sliderTrackHigh;

	@FindBy(id = "PrixProprietePlus")
	private WebElement pricePlusButton;

	// private static final String DOWN_PAYMENT_SECTION_CSS =
	// "#calculateur-pret-hypotecaire #zone_mise_de_fonds .inputSlide-ia";

	@FindBy(id = "MiseDeFondPlus")
	private WebElement downPaymentPlusButton;

	@FindBy(id = "TauxInteret")
	private WebElement interestRateInputField;

	@FindBy(id = "btn_calculer")
	private WebElement calculateButton;

	@FindBy(css = ".panneau-info:not(#calculateur)")
	private WebElement resultSection;

	@FindBy(id = "paiement-resultats")
	private WebElement resultLabel;

	private final WebDriver driver;

	public MortgagePaymentCalculatorPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@Step("Read current URL")
	@Attachment("Mortgage Payment Calculator page URL")
	public String getUrl() {
		return this.driver.getCurrentUrl();
	}

	@Step("Check if Slider handle is shown")
	private boolean isSliderHandleShown() {
		return sliderHandle.isDisplayed();
	}

	@Step("Read the Purchase Price Slider handle X coordinate")
	@Attachment("X coordinate")
	public int getPriceSliderHandleXCoordinate() {
		return sliderHandle.getLocation().getX();
	}

	@Step("Move the Purchase Price Slider to the right")
	public MortgagePaymentCalculatorPage moveSliderHandleToTheRight() {
		waitUntilPurchaseSliderHandleIsShown();

		getPurchasePriceSectionIntoView();

		holdOnFor(3000);
		new Actions(driver).dragAndDrop(sliderHandle, pricePlusButton).perform();
		holdOnFor(3000);
		return this;
	}

	@Step("Set the Purchase Price: {0}")
	public MortgagePaymentCalculatorPage setPurchasePrice(final int price) {
		waitUntilPurchaseSliderHandleIsShown();
		getPurchasePriceSectionIntoView();
		for (int i = 0; i < price / 250_000; i++)
			clickPurchasePricePlusButton();
		return this;
	}

	@Step("Set the Down Payment: {0}")
	public MortgagePaymentCalculatorPage setDownPayment(final int payment) {
		for (int i = 0; i < payment / 100_000; i++)
			clickDownPaymentPlusButton();
		return this;
	}

	@Step("Set Inteterest Rate: {0}")
	public MortgagePaymentCalculatorPage setInterestRate(double interestRate) {
		clearInterestRateInputFieldAndType(String.valueOf(interestRate));
		return this;
	}

	@Step("Calculate")
	public MortgagePaymentCalculatorPage calculate() {
		clickCalculateButton();
		return this;
	}

	@Step("Read calculated result")
	@Attachment("Result")
	public double getCalculatedResult() {
		String result = readCalculatedResult();
		result = result.replaceAll("\\$", "").replaceAll(",", "").trim();
		return Double.parseDouble(result);
	}

	@Step("Calculate and Read caluclated result")
	public double calculateAndGetCalculatedResult() {
		calculate();
		holdOnFor(2000);
		getCalculatedResultSectionIntoView();
		return getCalculatedResult();
	}

	@Step("Click the plus (+) button on the Purchase Price Slider")
	private void clickPurchasePricePlusButton() {
		pricePlusButton.click();
	}

	@Step("Click the plus (+) button on the Down Payment Slider")
	private void clickDownPaymentPlusButton() {
		downPaymentPlusButton.click();
	}

	@Step("Clear 'Interest Rate' input field and type: <{0}>")
	private void clearInterestRateInputFieldAndType(String interestRate) {
		interestRateInputField.clear();
		interestRateInputField.sendKeys(interestRate);
	}

	@Step("Click 'Calculate' Button")
	private void clickCalculateButton() {
		// calculateButton.click();
		JavascriptExecutor.class.cast(driver).executeScript("arguments[0].click();", calculateButton);
	}

	@Step("Read calculated result")
	@Attachment("Result")
	private String readCalculatedResult() {
		waitUntilCalculatedResultAppears();
		return resultLabel.getText();
	}

	private void waitUntilCalculatedResultAppears() {
		new WebDriverWait(driver, 10).ignoring(NoSuchElementException.class)
				.until((WebDriver d) -> resultLabel.isDisplayed());
		new WebDriverWait(driver, 10).ignoring(NoSuchElementException.class)
				.until((WebDriver d) -> !"".equals(resultLabel.getText()));
	}

	private void waitUntilPurchaseSliderHandleIsShown() {
		new WebDriverWait(driver, 10).ignoring(NoSuchElementException.class)
				.until((WebDriver d) -> isSliderHandleShown());
	}

	private void getPurchasePriceSectionIntoView() {
		intoView(purchasePriceSection);
	}

	private void getCalculatedResultSectionIntoView() {
		intoView(resultSection);
	}

	private void intoView(WebElement element) {
		JavascriptExecutor.class.cast(driver).executeScript("arguments[0].scrollIntoView(true)", element);
	}

	private void holdOnFor(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (Exception e) {
		}
	}

}
