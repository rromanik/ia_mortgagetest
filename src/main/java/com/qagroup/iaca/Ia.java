package com.qagroup.iaca;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.annotations.Step;

public class Ia implements IWebApp {

	private static final String BASE_URL = "http://www.ia.ca";

	private WebDriver driver;

	@Override
	@Attachment(value = "{0}")
	public byte[] getScreenshot(String name) {
		return TakesScreenshot.class.cast(driver).getScreenshotAs(OutputType.BYTES);
	}

	public void close() {
		if (this.driver != null)
			this.driver.quit();
		this.driver = null;
	}

	@Step("Open Start page: " + BASE_URL)
	public IndividualsPage openStartPage() {
		driver = Browser.getDriver();
		driver.get(BASE_URL);

		return new IndividualsPage(driver);
	}

}
