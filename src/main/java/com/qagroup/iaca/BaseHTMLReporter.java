package com.qagroup.iaca;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.internal.IResultListener2;

public class BaseHTMLReporter implements IResultListener2 {

	@Override
	public void onTestStart(ITestResult result) {
		try {
			System.out.println("Start: " + result.getName());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		createScreenshot(result);
	}

	@Override
	public void onTestFailure(ITestResult result) {
		createScreenshot(result);
		try {
			System.out.println("Failed: " + result.getInstanceName() + " : " + result.getName());
		} catch (Exception ignored) {
			System.out.println("In BaseHTMLReporter#onTestFailure");
		}
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		createScreenshot(result);
		try {
			System.out.println("Skipped: " + result.getInstanceName() + " : " + result.getName());
		} catch (Exception ignored) {
			System.out.println("In BaseHTMLReporter#onTestSkipped");
		}
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}

	@Override
	public void onStart(ITestContext context) {
		try {
			System.out.println("Start test: " + context.getName());
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#onStart:\n" + e.getMessage());
		}
	}

	@Override
	public void onFinish(ITestContext context) {
		try {
			System.out.println("Finish: " + context.getName());
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#onFinish:\n" + e.getMessage());
		}
	}

	private void createScreenshot(ITestResult result) {
		try {
			IWebAppTest.class.cast(result.getInstance()).getTestedAppInstance().getScreenshot("Page Screenshot");
		} catch (Exception e) {
			System.out.println("In BaseHTMLReporter#createScreenshot:\n" + e.getMessage());
		}
	}

	@Override
	public void onConfigurationSuccess(ITestResult itr) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConfigurationFailure(ITestResult itr) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConfigurationSkip(ITestResult itr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeConfiguration(ITestResult tr) {
		// TODO Auto-generated method stub

	}
}
