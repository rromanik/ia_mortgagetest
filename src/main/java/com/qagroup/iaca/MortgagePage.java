package com.qagroup.iaca;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class MortgagePage {

	private final WebDriver driver;

	@FindBy(css = ".icone-calculateur [data-utag-name='calculate_your_payments']")
	private WebElement calculateYourPaymentsButton;

	public MortgagePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		waitUntilLoaded();
	}

	@Step("Navigate to Mortgage Payment Calculator page")
	public MortgagePaymentCalculatorPage calculateYourPayments() {
		clickCalculateYourPaymentsButton();
		return new MortgagePaymentCalculatorPage(this.driver);
	}

	@Step("Click \"Calculate your payments\" button")
	private void clickCalculateYourPaymentsButton() {
		JavascriptExecutor.class.cast(driver).executeScript("arguments[0].scrollIntoView(true)",
				calculateYourPaymentsButton);
		calculateYourPaymentsButton.click();
	}

	private void waitUntilLoaded() {
		try {
			new WebDriverWait(driver, 10).ignoring(NoSuchElementException.class)
					.until(ExpectedConditions.visibilityOf(calculateYourPaymentsButton));
		} catch (TimeoutException e) {
			throw new IllegalStateException(
					"\n'Calculate your payments' button should be displayed on Mortgage page.\n", e);
		}
	}
}
