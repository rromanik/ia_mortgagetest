package com.qagroup.iaca;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class IndividualsPage {
	private final WebDriver driver;

	@FindBy(id = "topLangMenuItem")
	private WebElement lang;

	@FindBy(css = "[data-utag-name='loans']")
	private WebElement loansButton;

	@FindBy(css = "[data-utag-name='mortgage_loan']")
	private WebElement mortgagesLink;

	public IndividualsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
		waitForEnglishLanguige();
	}

	@Step("Navigate to Mortgage page")
	public MortgagePage navigateToMortgagePage() {
		clickLoansButton();
		clickMortgagesLink();
		return new MortgagePage(driver);
	}

	@Step("Click LOANS button")
	private void clickLoansButton() {
		waitUntilLoansButtonIsDisplayed();
		loansButton.click();
	}

	@Step("Click Mortgages link")
	private void clickMortgagesLink() {
		mortgagesLink.click();
	}

	private void waitForEnglishLanguige() {
		new WebDriverWait(driver, 15).until((WebDriver d) -> lang.getText().equals("FR"));
	}

	private void waitUntilLoansButtonIsDisplayed() {
		new WebDriverWait(driver, 15).ignoring(NoSuchElementException.class)
				.until((WebDriver d) -> loansButton.isDisplayed());
	}
}
