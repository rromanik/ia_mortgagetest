package com.qagroup.iaca;

public interface IWebApp {

	public byte[] getScreenshot(String name);

	/**
	 * Does nothing with the application for a time specified by {@code aWhile}
	 * in milliseconds.
	 * 
	 * @param aWhile
	 *            time to hold in milliseconds
	 */
	public default void holdOnFor(long aWhile) {
		try {
			Thread.sleep(aWhile);
		} catch (InterruptedException e) {
			// Just in case
			e.printStackTrace();
		}
	}
}
