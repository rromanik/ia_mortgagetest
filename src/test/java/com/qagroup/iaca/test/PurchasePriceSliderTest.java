package com.qagroup.iaca.test;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.qagroup.iaca.IWebApp;
import com.qagroup.iaca.IWebAppTest;
import com.qagroup.iaca.Ia;
import com.qagroup.iaca.MortgagePaymentCalculatorPage;

import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Purchase Price Slider")
public class PurchasePriceSliderTest implements IWebAppTest {
	private Ia iA = new Ia();

	private MortgagePaymentCalculatorPage mortgagePaymentCalculatorPage;

	private static final String METHOD1 = "testUrlOnPaymentCalculatorPage";
	
	@Test
	public void testUrlOnPaymentCalculatorPage() {
		mortgagePaymentCalculatorPage = iA.openStartPage().navigateToMortgagePage().calculateYourPayments();
		iA.holdOnFor(6000);
		Assert.assertEquals(mortgagePaymentCalculatorPage.getUrl(), "https://ia.ca/mortgage-payment-calculator",
				"\nCurrent URL is incorrect:\n");
	}

	@Stories("Slider Movement")
	@Test(dependsOnMethods = METHOD1)
	public void testSliderMovement() {
		int initialXCoordinate = mortgagePaymentCalculatorPage.getPriceSliderHandleXCoordinate();
		mortgagePaymentCalculatorPage.moveSliderHandleToTheRight();
		int finalXCoordinate = mortgagePaymentCalculatorPage.getPriceSliderHandleXCoordinate();

		Assert.assertNotEquals(finalXCoordinate, initialXCoordinate,
				"\nInitial and final horizontal positions of price slide handler remains the same after moving.\n");
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		iA.close();
	}

	@Override
	public IWebApp getTestedAppInstance() {
		return this.iA;
	}
}
