package com.qagroup.iaca.test;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.qagroup.iaca.IWebApp;
import com.qagroup.iaca.IWebAppTest;
import com.qagroup.iaca.Ia;
import com.qagroup.iaca.MortgagePaymentCalculatorPage;

import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

@Features("Mortgage Calculator")
@Stories("Payment Calculation")
public class MortgagePaymentCalculatorTest implements IWebAppTest {
	private Ia iA = new Ia();
	private MortgagePaymentCalculatorPage mortgagePaymentCalculatorPage;

	@BeforeClass
	public void navigateToCalculatorPage() {
		mortgagePaymentCalculatorPage = iA.openStartPage().navigateToMortgagePage().calculateYourPayments();
		iA.holdOnFor(3000);
	}

	@Test
	public void testMortgagePaymentCalculator() {
		mortgagePaymentCalculatorPage.setPurchasePrice(500_000);
		mortgagePaymentCalculatorPage.setDownPayment(100_000);
		mortgagePaymentCalculatorPage.setInterestRate(5.00);
		iA.holdOnFor(3000);
		double actualResult = mortgagePaymentCalculatorPage.calculateAndGetCalculatedResult();
		double expectedResult = 836.75;

		Assert.assertEquals(actualResult, expectedResult, 0.01, "Incorrect calculated payments:");
	}

	
	
	
	
	
	
	
	
	
	
	
	@AfterClass(alwaysRun = true)
	public void tearDown() {
		iA.close();
	}

	@Override
	public IWebApp getTestedAppInstance() {
		return this.iA;
	}
}
